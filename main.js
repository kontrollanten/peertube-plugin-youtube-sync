async function register ({
  getRouter,
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager
}) {
  registerSetting({
    name: 'youtube-api-key',
    label: 'Youtube API key',
    type: 'input',
  });

  registerSetting({
    name: 'youtube-channel-id',
    label: 'Youtube channel id',
    type: 'input',
  });

  const router = getRouter();

  router.get('/sync', async (req, res) => {
    const apiKey = await settingsManager.getSetting('youtube-api-key');
    const ytChannelId = await settingsManager.getSetting('youtube-channel-id');

     if (res.locals.authenticated === false) {
       return res
         .status(401)
         .json({
           message: 'Unauthenticated',
         });
     }

     res.json({
       message: 'res.locals.authenticated is undefined, which means you are authenticated',
       apiKey,
       ytChannelId,
     });
  })

  // console.log(router.stack);
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
